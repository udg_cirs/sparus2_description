# Changelog

## [24.1.0] - 30-01-2024

* Updated for release 24.1.0

## [20.10.0] - 23-10-2020

* Adapted configuration for new release

## [3.2.0] - 22-10-2019

* Applied cola2 lib refactor changes

## [3.1.0] - 25-02-2019

* First release
